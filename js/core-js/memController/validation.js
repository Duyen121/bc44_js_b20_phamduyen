var accNotiArr = [
  "Vui lòng nhập Số tài khoản",
  "Vui lòng nhập Số tài khoản gồm 4-6 ký tự số",
  "Số tài khoản đã tồn tại",
];
var nameNotiArr = [
  "Vui lòng nhập Họ tên",
  "Vui lòng nhập Họ tên chứa ký tự bảng chữ cái",
  "Vui lòng nhập Họ tên đầy đủ của bạn",
];
var emailNotiArr = ["Vui lòng nhập Email", "Vui lòng nhập Email hợp lệ"];
var passwordNotiArr = [
  "Vui lòng nhập Mật khẩu",
  "Vui lòng nhập Mật khẩu 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa & in thường, 1 ký tự đặc biệt)",
];
var workingDayNotiArr = [
  "Vui lòng nhập Ngày làm",
  "Vui lòng nhập Ngày làm theo thứ tự tháng/ngày/năm",
];
var basicSalNotiArr = [
  "Vui lòng nhập Lương cơ bản",
  "Vui lòng nhập Mức lương từ 1 triệu đến 20 triệu VND",
];
var positionNotiArr = ["Vui lòng nhập Chức vụ"];
var workingHourNotiArr = [
  "Vui lòng nhập Số giờ làm",
  "Số giờ làm cần trong khoảng 80-200h",
];

var dataJSON = localStorage.getItem("LOCAL_DATA");

var getAccNumsFormLocal = function () {
  var dataArr = JSON.parse(dataJSON);
  var accNumsArr = [];
  for (var i = 0; i < dataArr.length; i++) {
    var item = dataArr[i];
    var accNums = item.acc;
    accNumsArr.push(accNums);
  }
  return accNumsArr;
};

var getElement = function (tagId) {
  return document.getElementById(tagId);
};

var showMessage = function (notiTag, message) {
  notiTag.style.display = "block";
  notiTag.innerHTML = `${message}`;
};

var isAccNotDuplicated = function () {
  var accVal = getElement("tknv").value;
  var accNoti = getElement("tbTKNV");
  var existedAccNums = getAccNumsFormLocal();

  for (var i = 0; i < existedAccNums.length; i++) {
    if (accVal == existedAccNums[i]) {
      showMessage(accNoti, accNotiArr[2]);
      return false;
    }
  }
  showMessage(accNoti, "");
  return true;
};

var validateAccNum = function () {
  var accVal = getElement("tknv").value;
  var accNoti = getElement("tbTKNV");

  var validDigitsRegex = /^[0-9]{4,6}$/;

  if (accVal.length == 0) {
    showMessage(accNoti, accNotiArr[0]);
    return false;
  }

  if (!accVal.match(validDigitsRegex)) {
    showMessage(accNoti, accNotiArr[1]);
    return false;
  }

  showMessage(accNoti, "");
  return true;
};

var validateFullName = function () {
  var nameVal = getElement("name").value;
  var nameNoti = getElement("tbTen");
  var fullNameRegex = /^[a-zA-Z]+(?:\s[a-zA-Z]+)+$/;
  var notStringRegex = /^\+?[0-9 \.-]+$/;

  if (nameVal.length == 0) {
    showMessage(nameNoti, nameNotiArr[0]);
    return false;
  }
  if (nameVal.match(notStringRegex)) {
    showMessage(nameNoti, nameNotiArr[1]);
    return false;
  }
  if (!nameVal.match(fullNameRegex)) {
    showMessage(nameNoti, nameNotiArr[2]);
    return false;
  }

  showMessage(nameNoti, "");
  return true;
};

var validateEmail = function () {
  var emailVal = getElement("email").value;
  var emailNoti = getElement("tbEmail");

  var emailRegex =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

  if (emailVal.length == 0) {
    showMessage(emailNoti, emailNotiArr[0]);
    return false;
  }
  if (!emailVal.match(emailRegex)) {
    showMessage(emailNoti, emailNotiArr[1]);
    return false;
  }
  showMessage(emailNoti, "");
  return true;
};

var validatePassword = function () {
  var passwordVal = getElement("password").value;
  var passwordNoti = getElement("tbMatKhau");

  var passwordRegex =
    /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,10}$/;

  if (passwordVal.length == 0) {
    showMessage(passwordNoti, passwordNotiArr[0]);
    return false;
  }
  if (!passwordVal.match(passwordRegex)) {
    showMessage(passwordNoti, passwordNotiArr[1]);
    return false;
  }
  showMessage(passwordNoti, "");
  return true;
};

var validateWorkingDay = function () {
  var workingDayVal = getElement("datepicker").value;
  var workingDayNoti = getElement("tbNgay");

  var dateFormat = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;

  if (workingDayVal.length == 0) {
    showMessage(workingDayNoti, workingDayNotiArr[0]);
    return false;
  }
  if (!workingDayVal.match(dateFormat)) {
    showMessage(workingDayNoti, workingDayNotiArr[1]);
    return false;
  }
  showMessage(workingDayNoti, "");
  return true;
};

var validateBasicSal = function () {
  var basicSalVal = getElement("luongCB").value;
  var basicSalNoti = getElement("tbLuongCB");

  if (basicSalVal.length == 0) {
    showMessage(basicSalNoti, basicSalNotiArr[0]);
    return false;
  }
  if (basicSalVal < 1e6 || basicSalVal > 20e6) {
    showMessage(basicSalNoti, basicSalNotiArr[1]);
    return false;
  }
  showMessage(basicSalNoti, "");
  return true;
};

var validatePosition = function () {
  var positionTag = getElement("chucvu");
  var positionNoti = getElement("tbChucVu");

  if (positionTag.selectedIndex == 0) {
    showMessage(positionNoti, positionNotiArr[0]);
    return false;
  } else {
    showMessage(positionNoti, "");
    return true;
  }
};

var validateWorkingHour = function () {
  var workingHourVal = getElement("gioLam").value;
  var workingHourNoti = getElement("tbGiolam");

  if (workingHourVal.length == 0) {
    showMessage(workingHourNoti, workingHourNotiArr[0]);
    return false;
  }
  if (workingHourVal < 80 || workingHourVal > 200) {
    showMessage(workingHourNoti, workingHourNotiArr[1]);
    return false;
  }
  showMessage(workingHourNoti, "");
  return true;
};

var validateForm = function () {
  if (
    !validateAccNum() ||
    !validateFullName() ||
    !validateEmail() ||
    !validatePassword ||
    !validateWorkingDay() ||
    // !validatePosition ||
    !validateWorkingHour()
  ) {
    return false;
  } else {
    return true;
  }
};
