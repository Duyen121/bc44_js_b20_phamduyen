function getMemberInfo() {
  var acc = document.getElementById("tknv").value;
  var fullName = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var password = document.getElementById("password").value;
  var workingDay = document.getElementById("datepicker").value;
  var basicSalary = document.getElementById("luongCB").value;
  var position = document.getElementById("chucvu").value;
  var workingHour = document.getElementById("gioLam").value;

  var memberInfo = new Member(
    acc,
    fullName,
    email,
    password,
    workingDay,
    basicSalary,
    position,
    workingHour
  );

  return memberInfo;
}

function displayMemberInfo(memberInfo) {
  document.getElementById("tknv").value = memberInfo.acc;
  document.getElementById("name").value = memberInfo.fullName;
  document.getElementById("email").value = memberInfo.email;
  document.getElementById("password").value = memberInfo.password;
  document.getElementById("datepicker").value = memberInfo.workingDay;
  document.getElementById("luongCB").value = memberInfo.basicSalary;
  document.getElementById("chucvu").value = memberInfo.position;
  document.getElementById("gioLam").value = memberInfo.workingHour;
}

function renderMemberInfo(memberList) {
  var HTMLContent = "";
  for (let i = 0; i < memberList.length; i++) {
    var memberInfo = memberList[i];
    var trContent = `<tr>
    <td>${memberInfo.acc}</td>
    <td>${memberInfo.fullName}</td>
    <td>${memberInfo.email}</td>
    <td>${memberInfo.workingDay}</td>
    <td>${memberInfo.position}</td>
    <td>${memberInfo.calGrossSal()}</td>
    <td>${memberInfo.evaluateEmployee()}</td>
    <td>
      <button onclick="deleteInfo(${
        memberInfo.acc
      })" class="btn btn-danger">Xoá</button>
      <button onclick="editInfo(${
        memberInfo.acc
      })" class="btn btn-secondary" data-toggle="modal" data-target="#myModal" >Sửa</button>
    </td>
    </tr>`;
    HTMLContent = HTMLContent + trContent;
  }
  document.getElementById("tableDanhSach").innerHTML = HTMLContent;
}
