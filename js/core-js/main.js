var memberList = [];
var dataJSON = localStorage.getItem("LOCAL_DATA");

if (dataJSON != null) {
  var dataArr = JSON.parse(dataJSON);
  for (var i = 0; i < dataArr.length; i++) {
    var item = dataArr[i];
    var memberInfo = new Member(
      item.acc,
      item.fullName,
      item.email,
      item.password,
      item.workingDay,
      item.basicSalary,
      item.position,
      item.workingHour
    );
    memberList.push(memberInfo);
  }
  renderMemberInfo(memberList);
}

function saveDataInLocalStor(data) {
  var dataJSON = JSON.stringify(data);
  localStorage.setItem("LOCAL_DATA", dataJSON);
}

function resetInfo() {
  document.getElementById("tknv").disabled = false;
  document.getElementById("password").disabled = false;

  $(".sp-thongbao").hide();
  $(":input", "#myModal").val("").prop("selectedIndex", 0);
  setDefaultDate();
}

function addNewMember() {
  var member = getMemberInfo();
  var isValid = validateForm();

  isValid = isValid & isAccNotDuplicated() & validatePosition();

  if (isValid) {
    memberList.push(member);
    renderMemberInfo(memberList);
    saveDataInLocalStor(memberList);
  } else {
    alert(
      "Thông tin điền trong form không hợp lệ. Vui lòng nhập lại thông tin"
    );
  }
}

function updateInfo() {
  var member = getMemberInfo();
  var location = memberList.findIndex(function (item) {
    return item.acc == member.acc;
  });

  var isValid = validateForm() & validatePosition();

  if (location != -1 && isValid) {
    memberList[location] = member;
    renderMemberInfo(memberList);
    saveDataInLocalStor(memberList);
  } else {
    alert(
      "Thông tin điền trong form không hợp lệ. Vui lòng nhập lại thông tin"
    );
  }
}

function editInfo(id) {
  $(".sp-thongbao").hide();
  var location = memberList.findIndex(function (item) {
    return item.acc == id;
  });
  if (location != -1) {
    document.getElementById("tknv").disabled = true;
    document.getElementById("password").disabled = true;
    displayMemberInfo(memberList[location]);
  }
}

function deleteInfo(id) {
  var location = -1;
  for (let i = 0; i < memberList.length; i++) {
    var item = memberList[i];
    if (item.acc == id) {
      location = i;
    }
  }
  memberList.splice(location, 1);
  renderMemberInfo(memberList);
  saveDataInLocalStor(memberList);
}

function searchMemberByRank() {
  var searchInputValue = document.getElementById("searchName").value;
  var searchingMemArr = memberList.filter(function (item) {
    var memberRank = item.evaluateEmployee();
    return memberRank.toLowerCase() == searchInputValue.toLowerCase();
  });

  if (searchingMemArr.length > 0) {
    renderMemberInfo(searchingMemArr);
  } else {
    alert(`Không có nhân viên nào xếp loại ${searchInputValue.toLowerCase()} `);
  }
}
