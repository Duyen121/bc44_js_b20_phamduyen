function Member(
  _acc,
  _fullName,
  _email,
  _password,
  _workingDay,
  _basicSalary,
  _position,
  _workingHour
) {
  this.acc = _acc;
  this.fullName = _fullName;
  this.email = _email;
  this.password = _password;
  this.workingDay = _workingDay;
  this.basicSalary = _basicSalary;
  this.position = _position;
  this.workingHour = _workingHour;
  this.calGrossSal = function () {
    var grossSalary = "";
    var vndFormat = new Intl.NumberFormat("vi-VN", {
      style: "currency",
      currency: "VND",
    });

    if (this.position == "Sếp") {
      grossSalary = this.basicSalary * 3;
      return vndFormat.format(grossSalary);
    } else if (this.position == "Trưởng phòng") {
      grossSalary = this.basicSalary * 2;
      return vndFormat.format(grossSalary);
    } else {
      grossSalary = this.basicSalary;
      return vndFormat.format(grossSalary);
    }
  };
  this.evaluateEmployee = function () {
    var ranking = "";
    if (this.workingHour < 160) {
      return (ranking = "Trung bình");
    } else if (this.workingHour < 176) {
      return (ranking = "Khá");
    } else if (this.workingHour < 192) {
      return (ranking = "Giỏi");
    } else {
      return (ranking = "Xuất sắc");
    }
  };
}
